# coding: utf8
from functions import add_bet, get_info, make_db, connect_to_db, disconnect_db

if __name__ == '__main__':
    # initial methods
    connect_to_db()
    make_db()

    # tests
    add_bet('Вася Пупкин', 10, 4)
    add_bet('Петя Васин', 6, 3)
    add_bet('Вася Пупкин', 20.5, 15)

    # tests info
    get_info()
    # close connection
    disconnect_db()
