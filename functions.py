# coding: utf8
import psycopg2


DB_HOST = 'localhost'
DB_NAME = 'testcasino'
DB_USER = 'postgres'
DB_PASSWORD = 'password'
CURSOR = None
CONNECTION = None


def add_bet(user_name, bet_sum, jackpot_sum):
    """
    Method to add user bet and jackpot to database.
    Create new user if 'user_name' doesnt exist.
    Bet and jackpot sums must be greater than 0

    :param user_name: name of user which want to make bet
    :param bet_sum: sum of user bet
    :param jackpot_sum: sum of user jackpot contribution
    """

    if not user_exist(user_name):
        make_user(user_name)

    if jackpot_sum > 0:
        add_user_bet('jackpot', jackpot_sum)

    if bet_sum > 0:
        add_user_bet(user_name, bet_sum)


def user_exist(user_name):
    """
    Check user name if exist in database column 'user_name'.
    Return boolean True or False

    :param user_name: name of user which we want to check in database
    :return: True if user exist, False if user doesnt exist
    """

    # TODO too long!!
    CURSOR.execute('SELECT exists (SELECT 1 FROM casino_test WHERE user_name = \'{}\' LIMIT 1)'.format(user_name))

    return CURSOR.fetchall()[0][0]


def make_user(user_name):
    """
    Create new entry in database. Commit changes.
    Print that new user is created.

    :param user_name: name of new user
    """

    CURSOR.execute(
        'INSERT INTO casino_test VALUES (\'{}\', 0);'.format(user_name)
    )
    CONNECTION.commit()
    print('New user created: {}'.format(user_name))


def add_user_bet(user_name, bet_sum):
    """
    Add user bet to database. Commit database changes.
    Print user name of bet and sum of bet.

    :param user_name: name of user to bet
    :param bet_sum: sum of user bet
    """

    # TODO too long!!
    CURSOR.execute(
        'UPDATE casino_test SET user_bet = user_bet + \'{}\' WHERE user_name = \'{}\''.format(bet_sum, user_name)
    )
    CONNECTION.commit()
    print('Bet accepted. {} bet {}'.format(user_name, bet_sum))


def get_info():
    """
    Print info about current database entries
    """

    CURSOR.execute('SELECT * FROM casino_test')
    print(CURSOR.fetchall())


def make_db():
    """
    Make database if it not exist.
    """

    # check if havent database connection
    if not CURSOR:
        print('Not connected to db')
        return

    # check if db is exist
    CURSOR.execute(
        'SELECT exists(SELECT * FROM information_schema.tables WHERE table_name=\'{}\')'.format('casino_test')
    )
    table_exist = CURSOR.fetchone()[0]

    if not table_exist:
        # create a new table with a single column called "name"
        CURSOR.execute(
            'CREATE TABLE casino_test (user_name text, user_bet real);'
        )
        # create a new user 'jackpot' with initial (zero) jackpot sum
        CURSOR.execute(
            'INSERT INTO casino_test VALUES (\'{}\', 0);'.format('jackpot')
        )
        # apply all creations
        CONNECTION.commit()
        print('New database created')


def connect_to_db():
    """
    Try to connect database with users parameters
    """
    # TODO make me!! unglobal
    global CURSOR
    global CONNECTION

    try:
        connect_str = 'host={} dbname={} user={} password={}'.format(
            DB_HOST, DB_NAME, DB_USER, DB_PASSWORD
        )
        # use our connection values to establish a connection
        CONNECTION = psycopg2.connect(connect_str)
        # create a psycopg2 cursor that can execute queries
        CURSOR = CONNECTION.cursor()
    except Exception as e:
        # print message if cant connect database
        print("Uh oh, can't connect. Invalid dbname, user or password?")
        print(e)


def disconnect_db():
    """
    Disconnect database
    """

    CONNECTION.close()
